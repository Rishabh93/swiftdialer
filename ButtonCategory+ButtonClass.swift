//
//  ButtonCategory+ButtonClass.swift
//  SwiftDialer
//
//  Created by Rishabh  on 09/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import Foundation
import UIKit
extension UIButton {
    func roundTopRightCorner(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners:UIRectCorner.TopRight, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.CGPath
        self.layer.mask = mask
        layer.addSublayer(mask)
    }
}
