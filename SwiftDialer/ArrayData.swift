//
//  ArrayData.swift
//  SwiftDialer
//
//  Created by Rishabh  on 03/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import Foundation
import UIKit
//class ArrayData: NSObject {
//    var array = ["One","Two","Three","Four","Five"]
//}
//class Singleton {
//    class var sharedInstance : Singleton {
//        struct Static {
//            static var onceToken: dispatch_once_t = 0
//            static var instance: Singleton? = nil
//        }
//        dispatch_once(&Static.onceToken) {
//            Static.instance = Singleton()
//        }
//        return Static.instance!
//    }
//}

class Singleton  {
    var ContactArray = [String]()
    static let sharedInstance = Singleton()
}