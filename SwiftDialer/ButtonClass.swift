//
//  ButtonClass.swift
//  SwiftDialer
//
//  Created by Rishabh  on 03/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import Foundation
import UIKit
class ButtonClass: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        layer.backgroundColor = UIColor.blueColor().CGColor
        layer.cornerRadius = 18
        layer.borderWidth = 2
        layer.borderColor = UIColor.blackColor().CGColor
    }
}
//@IBDesignable class ButtonClass: UIButton {
//    @IBInspectable var borderColor1: UIColor? = UIColor.clearColor() {
//        didSet {
//            layer.borderColor = self.borderColor1?.CGColor
//        }
//    }
//    @IBInspectable var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = self.borderWidth
//        }
//    }
//    
//    @IBInspectable var leftTopRadius: CGSize = CGSizeZero {
//        didSet {
//            var maskPath:UIBezierPath = UIBezierPath.init(roundedRect: layer.bounds, byRoundingCorners:UIRectCorner.TopLeft, cornerRadii: leftTopRadius)
//            var maskLayer: CAShapeLayer = CAShapeLayer()
//            //maskLayer.frame = self.bounds
//            maskLayer.path  = maskPath.CGPath
//            //layer.mask = maskLayer
//            
//            
//            self.layer.addSublayer(maskLayer)
//            //[self.viewOutlet.layer addSublayer:borderLayer];
////            layer.cornerRadius = self.cornerRadius
////            layer.masksToBounds = self.cornerRadius > 0
//        }
//    }


    
//    @IBInspectable var bottomRightRadius: CGSize = CGSizeZero {
//        didSet {
//            var maskPath:UIBezierPath = UIBezierPath.init(roundedRect: layer.bounds, byRoundingCorners:UIRectCorner.BottomRight, cornerRadii: bottomRightRadius)
//            var maskLayer: CAShapeLayer = CAShapeLayer()
//            //maskLayer.frame = self.bounds
//            maskLayer.path  = maskPath.CGPath
//            //layer.mask = maskLayer
//            
//            
//            self.layer.addSublayer(maskLayer)
//            //[self.viewOutlet.layer addSublayer:borderLayer];
//            //            layer.cornerRadius = self.cornerRadius
//            //            layer.masksToBounds = self.cornerRadius > 0
//        }
//    }
    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//    }
    
//    override func drawRect(rect: CGRect) {
//        self.layer.cornerRadius = self.cornerRadius
//        self.layer.borderWidth = self.borderWidth
//        self.layer.borderColor = self.borderColor?.CGColor
//    }
