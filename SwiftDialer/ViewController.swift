//
//  ViewController.swift
//  SwiftDialer
//
//  Created by Rishabh  on 03/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
//    var contactArray = [String]()
    var userInTheMiddle : Bool = false
    @IBOutlet weak var myLabel: UILabel!
    override func viewDidLoad() {
    super.viewDidLoad()
        
    // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func funcForButton(sender: AnyObject) {
        let title = sender.currentTitle
        if userInTheMiddle == false{
            myLabel.text = title
            userInTheMiddle = true
        } else {
        myLabel.text = myLabel.text! + (title ?? "")
        }
        
    }
  
    @IBAction func funcForBackSpace(sender: AnyObject) {
//        let length = myLabel.text!.characters.count
        if myLabel.text!.characters.count != 0 {
        let rangeOfSubstring = myLabel.text!.startIndex..<myLabel.text!.endIndex.advancedBy(-1)
        let str = myLabel.text![rangeOfSubstring]
        myLabel.text! = str
        }
    }
    @IBAction func myCallButton(sender: UIButton) {
        let str = myLabel.text!
        let phone = "tel://\(str)";
        let url:NSURL = NSURL(string:phone)!;
        UIApplication.sharedApplication().openURL(url);
        Singleton.sharedInstance.ContactArray.append(phone)
    }
    
}

