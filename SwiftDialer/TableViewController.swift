//
//  TableViewController.swift
//  SwiftDialer
//
//  Created by Rishabh  on 03/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import Foundation
import UIKit
class TableViewController: UITableViewController {
//    var array = ["One","Two","Three","Four","Five"]
//    var arrayDataObj : ArrayData = ArrayData()
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return arrayDataObj.array.count
        return Singleton.sharedInstance.ContactArray.count

    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("CELL")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "CELL")
        }
        cell?.textLabel?.text = Singleton.sharedInstance.ContactArray[indexPath.row]
        return cell!
    }
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
    }

}